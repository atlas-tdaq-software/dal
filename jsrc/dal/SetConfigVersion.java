package dal;

public class SetConfigVersion
  {
    public static void main(final String[] args)
      {
        if (args.length != 2)
          {
            ers.Logger.error(new ers.Issue("wrong number of command line agruments\n" + "usage: java dal.SetConfigVersion \'partition\' \'version\'"));
            System.exit(1);
            return;
          }

        final String partition = args[0];
        final String version = args[1];

        try
          {
            dal.Algorithms.set_config_version(partition, version, true);
          }
        catch (final config.ConfigException ex)
          {
            if (dal.Algorithms.is_schema_changed(ex))
              ers.Logger.warning(ex);
            else
              ers.Logger.fatal(ex);
            System.exit(1);
          }
      }
  }